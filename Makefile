OUTPUT_DIR=build
DEBUG_LOG_PDF=$(OUTPUT_DIR)/debug-pdf.log
DEBUG_LOG_DOCX=$(OUTPUT_DIR)/debug-docx.log

TEX=lualatex
TEX_FLAGS=--output-dir=$(OUTPUT_DIR)

BIB=bibtex
BIB_FLAGS=

all: plos

setup:
	@echo "Setting up output directory:"
	@mkdir -p $(OUTPUT_DIR)
	# Complete

clean:
	@echo "Clearing build cache..."
	@rm -rf $(OUTPUT_DIR)/*
	@echo "Complete."
	# Complete

plos-paper: | setup plos-main.tex
	@echo "Making PLOS One PDF:"
	# Setup debug log
	@> $(DEBUG_LOG_PDF)
	# Generate temp output
	@$(TEX) $(TEX_FLAGS) plos-main.tex 2>&1 >> $(DEBUG_LOG_PDF)
	# Collect bib refs
	#@$(BIB) $(BIB_FLAGS) $(OUTPUT_DIR)/plos-main.aux 2>&1 >> $(DEBUG_LOG_PDF)
	# Put refs in bibliography
	@$(TEX) $(TEX_FLAGS) plos-main.tex 2>&1 >> $(DEBUG_LOG_PDF)
	# Link in-text refs to bibliography numbering
	@$(TEX) $(TEX_FLAGS) plos-main.tex 2>&1 >> $(DEBUG_LOG_PDF)
	# Complete

plos-appendix: | setup plos-appendix.tex
	@echo "Making PLOS One Appendix PDF:"
	# Setup debug log
	@> $(DEBUG_LOG_PDF)
	# Generate temp output
	@$(TEX) $(TEX_FLAGS) plos-appendix.tex 2>&1 >> $(DEBUG_LOG_PDF)
	# Collect bib refs
	@$(BIB) $(BIB_FLAGS) $(OUTPUT_DIR)/plos-appendix.aux 2>&1 >> $(DEBUG_LOG_PDF)
	# Put refs in bibliography
	@$(TEX) $(TEX_FLAGS) plos-appendix.tex 2>&1 >> $(DEBUG_LOG_PDF)
	# Link in-text refs to bibliography numbering
	@$(TEX) $(TEX_FLAGS) plos-appendix.tex 2>&1 >> $(DEBUG_LOG_PDF)

# Create a compressed PDF
plos: | plos-paper plos-appendix $(OUTPUT_DIR)/plos-main.pdf $(OUTPUT_DIR)/plos-appendix.pdf
	@echo "Compressing PDF:"
	@gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer \
	-dDownsampleColorImages=true -dColorImageResolution=300 -dNOPAUSE -dQUIET \
	-dBATCH -sOutputFile=$(OUTPUT_DIR)/plos-compressed.pdf $(OUTPUT_DIR)/plos-main.pdf
	@gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer \
	-dDownsampleColorImages=true -dColorImageResolution=300 -dNOPAUSE -dQUIET \
	-dBATCH -sOutputFile=$(OUTPUT_DIR)/plos-app-compressed.pdf $(OUTPUT_DIR)/plos-appendix.pdf
	# Complete

# Create a monolithic bibtex file from all of the bibs
megabib: | setup bibs/*.bib
	@echo "Making mega-bib:"
	@mkdir -p $(OUTPUT_DIR)
	@> $(OUTPUT_DIR)/megabib.bib
	@for b in bibs/*.bib; do \
	    cat "$$b" >> $(OUTPUT_DIR)/megabib.bib ; \
	done
	# Complete

# Create a Word Document (DOCX)
docx: | megabib plos-main.tex $(OUTPUT_DIR)/megabib.bib
	@echo "Making docx:"
	# Setup debug log
	@> $(DEBUG_LOG_DOCX)
	@pandoc \
	-f latex \
	-t docx \
	--log=$(DEBUG_LOG_DOCX) \
	--quiet \
	--resource-path=. \
	--bibliography=$(OUTPUT_DIR)/megabib.bib \
	-o $(OUTPUT_DIR)/main.docx \
	plos-main.tex
	# Complete

tar: | setup
	@echo "Making tarball:"
	@tar -czf $(OUTPUT_DIR)/`basename \`pwd\``_`date "+%Y%m%d_%H%M%S"`.tar.gz Makefile *.tex assets/ bibs/
	# Complete
